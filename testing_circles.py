import numpy as np
import math
import matplotlib.pyplot as plt

class Point:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
    
def sineAroundCircle( cx, cy, radius, amplitude, angle, frequency):
        p = Point()
        p.x = cx+(radius+amplitude*np.sin(frequency*angle))*np.cos(angle)
        p.y = cy+(radius+amplitude*np.sin(frequency*angle))*np.sin(angle)
        return p

cx=1
cy=1
radius=100
amp=10
frequency=40
angle= 30 * math.pi/180
pt= Point()

fig, ax = plt.subplots()
pt=sineAroundCircle(cx,cy,radius,amp,angle,frequency)
ax.plot(pt.x, pt.y, 'bo')
plt.show()