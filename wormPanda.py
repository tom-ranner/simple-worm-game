#!/usr/bin/env python
import random
import sys

from direct.gui.DirectGui import *
from direct.interval.IntervalGlobal import *
from direct.showbase.ShowBase import ShowBase
from fenics import Expression
from numpy import *
from panda3d.core import *
from panda3d.core import GeomNode
from simple_worm.controls import ControlsFenics as Controls
from simple_worm.util import v2f
from simple_worm.worm import Worm
from fenics import sqrt, dot, Dx, as_vector, project
from simple_worm.util import f2n

# Styling for instructions
def addInstructions(position, msg):
    return OnscreenText(
        text=msg,
        style=1,
        fg=(1, 1, 1, 1),
        scale=0.05,
        shadow=(0, 0, 0, 1),
        parent=base.a2dTopLeft,
        pos=position,
        align=TextNode.ALeft,
    )

f=open("centreOfWorm.txt", "w")

class WormDemo(ShowBase):
    def postTitle(self):
        # On screen title
        self.title = OnscreenText(
            text="Panda3D:Worm",
            parent=base.a2dBottomCenter,
            fg=(1, 1, 1, 1),
            shadow=(0, 0, 0, 0.5),
            pos=(0, 0.1),
            scale=0.1,
        )

    
    def postInstructions(self):
        # Post the instructions
        line_size=0.06 #how far appart each line of the instructions is separated by
        self.inst1 = addInstructions((0.08, -line_size, -0.04), "[H]: START GAME")
        self.inst2 = addInstructions((0.08, -line_size*2, -0.04), "[left arrow]: Rotate Worm Left")
        self.inst3 = addInstructions((0.08, -line_size*3, -0.04), "[right arrow]: Rotate Worm Right")
        self.inst4 = addInstructions((0.08, -line_size*4, -0.04), "[left arrow]: Rotate Worm Upwards")
        self.inst5 = addInstructions((0.08, -line_size*5, -0.04), "[right arrow]: Rotate Worm Downwards")
        self.inst6 = addInstructions((0.08, -line_size*6, -0.04), "[W]: Zoom Out")
        self.inst7 = addInstructions((0.08, -line_size*7, -0.04), "[S]: Zoom In")
        self.inst8 = addInstructions((0.08, -line_size*8, -0.04), "[A]: Rotate Camera Left")
        self.inst9 = addInstructions((0.08, -line_size*9, -0.04), "[D]: Rotate Camera Right")
        self.inst10 = addInstructions((0.08, -line_size*10, -0.04), "[Z]: Rotate Camera Upwards")
        self.inst11 = addInstructions((0.08, -line_size*11, -0.04), "[X]: Rotate Camera Downwards")
    
    def removeInstructions(self):
        #removes the instructions
        self.inst1.destroy()
        self.inst2.destroy()
        self.inst3.destroy()
        self.inst4.destroy()
        self.inst5.destroy()
        self.inst6.destroy()
        self.inst7.destroy()
        self.inst8.destroy()
        self.inst9.destroy()
        self.inst10.destroy()
        self.inst11.destroy()

    def postScore(self):
        #posts the score
        self.scorePost= addInstructions((0.08, -0.06, -0.04), f'Score: {self.score}')
    
    def keyCommands(self):
        # This is used to store which keys are currently pressed.
        self.keyMap = {
            "cam_left": 0,
            "cam_right": 0,
            "zoom_in": 0,
            "zoom_out": 0,
            "look_up": 0,
            "look_down": 0,
            "start_game": 0,
            "mup": 0,
            "mright": 0,
            "mleft": 0,
            "mdown": 0,
        }

        #tells the programme to accespt these key commands and what to do when they are pressed
        self.accept("escape", sys.exit)
        self.accept("a", self.setKey, ["cam_left", True])
        self.accept("d", self.setKey, ["cam_right", True])
        self.accept("s", self.setKey, ["zoom_in", True])
        self.accept("w", self.setKey, ["zoom_out", True])
        self.accept("z", self.setKey, ["look_up", True])
        self.accept("x", self.setKey, ["look_down", True])
        self.accept("h", self.setKey, ["start_game", True])
        self.accept("arrow_left", self.setKey, ["mleft", True])
        self.accept("arrow_down", self.setKey, ["mdown", True])
        self.accept("arrow_right", self.setKey, ["mright", True])
        self.accept("arrow_up", self.setKey, ["mup", True])

        #Tells the programme to stop doing things once the keys are not being pressed
        self.accept("a-up", self.setKey, ["cam_left", False])
        self.accept("s-up", self.setKey, ["zoom_in", False])
        self.accept("w-up", self.setKey, ["zoom_out", False])
        self.accept("d-up", self.setKey, ["cam_right", False])
        self.accept("z-up", self.setKey, ["look_up", False])
        self.accept("x-up", self.setKey, ["look_down", False])
        self.accept("h-up", self.setKey, ["start_game", False])
        self.accept("arrow_left-up", self.setKey, ["mleft", False])
        self.accept("arrow_down-up", self.setKey, ["mdown", False])
        self.accept("arrow_right-up", self.setKey, ["mright", False])
        self.accept("arrow_up-up", self.setKey, ["mup", False])
        taskMgr.add(self.move, "moveTask")

    def lighting(self):
        # Directional light that looks towards the ground
        directionalLight = DirectionalLight("directionalLight")
        directionalLight.setColor((1, 1, 1, 1))
        self.directionalLightNP = render.attachNewNode(directionalLight) #attatches the node path to the scene graph
        self.directionalLightNP.setPos(0.0, 60.0, 0.0) #sets the position (x,y,z) of the origin of the light
        self.directionalLightNP.setHpr(180, -90, 0) #sets the orientation of the light- the direstion it is pointing (Heading, pitch, roll)
        render.setLight(self.directionalLightNP)

        # Lighting for whole scene so that the sky can be visible
        ambient = AmbientLight("ambient")
        ambient.setColor((0.05, 0.05, 0.2, 1))
        ambientNodePath = render.attachNewNode(ambient)
        render.setLight(ambientNodePath)
    
    def wormMaterial(self):
         # Material of the worm
        self.myMaterial = Material()
        # Properties for obsedian masterial
        self.myMaterial.setShininess(0.3 * 128)
        self.myMaterial.setAmbient((0.05375, 0.05, 0.06625, 1))
        self.myMaterial.setSpecular((0.332741, 0.328634, 0.346435, 1))
    
    def loadGround(self):
        # load the ground model
        ground = loader.loadModel("models/sand/snd.bam")
        ground.setPos(0, 0.0, -0.2)
        ground.reparentTo(render)
    
    def loadSky(self):
         # load the sky model
        sky= loader.loadModel("models/sky/sky.bam")
        sky.setPos(-1, -1, -80)
        sky.reparentTo(render)
    
    def rotate_coordinate(self, x, y, a, b, theta):
        #rotates the point (x,y) about the coordinate (a,b) by an angle of theta anti clockwise. Theta is the angle that the 
        # line of the centre of the worm makes with the x axis ('H') 
        x_prime=((x-a)*cos(theta)-(y-b)*sin(theta))+a
        y_prime=((y-b)*cos(theta)+(x-a)*sin(theta))+b
        return(x_prime, y_prime)

    def is_object_touching_bug(self, bug_coordinate, wormCoordinate ,theta, width, length, height, radius=0):
        #works out if the coordinate of the worm, wormCoordinate (which represents a single point on the worm) is touching the worm
        #by first rotating this coordinate about the coordinate of the centre of the beetle, bug_coordinate, and then chekcing if this
        #is within a 2D 'box' of dimensions [1*0.5] surrounding the centre of the bug
        Z=wormCoordinate[2]
        X, Y=self.rotate_coordinate(wormCoordinate[0], wormCoordinate[1], bug_coordinate[0], bug_coordinate[1], theta)
        #(X, Y, Z) are the rotated x,y,z coordinates of the worm 
        if ((bug_coordinate[0]-length/2-radius < X < bug_coordinate[0]+length/2+radius) and #within extreme x coorinates
            (bug_coordinate[1]-width/2-radius< Y < bug_coordinate[1]+width/2+radius) #within extreme y coorinates
            and (bug_coordinate[2]-height/2 < Z < bug_coordinate[2]+height/2)): #within extreme z coorinates
            return True #This method returns true if the object is touching the bug and false if not.
        return False  

    def check_positions(self, object, Radius):
        #A method that checks if the object (ie bug or apple) is touching an existing bug or apple. 
        touching_apple=True
        while touching_apple==True:    
            initial_object_pos= object.getPos()
            touching_apple=False
            for index, bug in self.larvaePositions.items():
                while (self.is_object_touching_bug( bug[1],  object.getPos() ,bug[2], self.bugWidth, self.bugLength, self.bugHeight, Radius) == True):
                    #While the object is touching a bug, generate a new random position
                    object.setPos(random.randint(-self.spawnRadius, self.spawnRadius),random.randint(-self.spawnRadius, self.spawnRadius), random.randint(0, 3))
                    #checking if this new bug position coollides with one of the apples by looping through the apple positions
                    for index, apple in self.applePositions.items():
                        if linalg.norm(object.getPos()-apple[1])<0.3:
                            touching_apple=True
                    #Once we have gone through all apple positions, then touching_apple is only true if the new position is touching an existing apple
                    #So if it is true, then set the position back to its original position, which makes the while loop run again.
                    if touching_apple == True:
                        self.apple.setPos(initial_object_pos)
            #we now have a position that doesnt collide with a bug, but if it never collided with a bug in the first place, then it could still
            #be colliding wiht an apple. So we need to check the apple coordinates, and if these are touchign the positons, then we need to go throught
            #the entire while loop again with a new random position
            for index, apple in self.applePositions.items():
                if linalg.norm(object.getPos()-apple[1])<0.3:
                    object.setPos(random.randint(-self.spawnRadius, self.spawnRadius),random.randint(-self.spawnRadius, self.spawnRadius), random.randint(0, 3))
                    touching_apple=True


    
    def addApple(self, key):
        #add another apple which will have the key 'key' in the dictionary self.applePositions
        self.apple = loader.loadModel("apple4.bam")
        self.apple.setScale(0.3, 0.3, 0.3)
        self.apple.setHpr(random.randint(0,360), 0, 0) #random orientation in x-y direction, but upright
        self.apple.setPos(random.randint(-self.spawnRadius, self.spawnRadius),random.randint(-self.spawnRadius, self.spawnRadius), random.randint(0, 3))
        self.check_positions(self.apple, 3)

        self.apple.reparentTo(render)

        #adds this apple to the dictionary containing info about the apple, where {key:value}={i:[node, coordinate]}
        self.applePositions[key]=[self.apple, self.apple.getPos()]
        

    def load_Initial_Apples(self):
        self.applePositions={} #a dictionary that holds information about each apple, where {key:value}={index:[node, coordinate]}
        #so each key value is a list containing the node of that apple and its coordinates

        for i in range(0,5):#creates an initial 5 apples
            self.addApple(i)
    
    
    def loadLarvae(self, i):
        #loads model of the larvae
        bug = loader.loadModel("larvae2.bam") #Initially, a larvae is loaded with same orientation and position as the bug that will appear
        bug.setScale(0.05, 0.05, 0.05) #The larvae will start off as a small size and will grow
        #gives the larvae a random orientation in the x-y plane
        bug.setHpr(random.randint(0,180), 180, 90)
        #gives larvae a random coordinate
        bug.setPos(random.randint(-self.spawnRadius, self.spawnRadius),random.randint(-self.spawnRadius, self.spawnRadius), random.randint(0, 3)) 

        #checks if the larvae in question is touching a bug/larvae or an apple and generates new positions if this is the case
        self.check_positions(bug, 2)
        bug.reparentTo(render) 
        #appends the final node to a dictionary that can also be used to access info about each bug, including its position and orientation
        self.larvaePositions[i]=[bug, bug.getPos(), bug.getH(), "larvae", "not moved"] 

    def loadBug(self, larvae, key, position, angle): #a method that turns the larvae into a bug
        larvae.removeNode()#removes the larvae model because otherwise it would show beneath the bug model
        
        bug = loader.loadModel("bug3.bam") #we now import a new model and give it to a new variable name 
        bug.setScale(0.2,0.2,0.2) #this is the size of all bugs- they do not grow once they are bugs
        bug.setHpr(angle, 180, 90) #has the same orientation as the larvae in its place
        bug.setPos(position[0], position[1], position[2]) #has the same position as the larvae it is replacing
        bug.reparentTo(render)

        self.larvaePositions[key]=[bug, position, angle, "bug", "not moved"] #replaces the old value in the dictionary with the new bug so that its postiion and angles
        #can be accessed for checking for collisions
    
    def __init__(self):
        self.number_of_circles= 30 #the number of points that the centre of the worm is divided up into 
        
        #dimensions of the head and tail in relation to the number of circles that make up the entire snake
        self.end_of_head=29
        self.start_of_head=27 #this means the first five circles make up the very end of the head of the snake etc
        self.start_of_tail=5 

        self.bugHeight=0.5 #dimensions of the 'box' that encases the bug
        self.bugWidth=0.5
        self.bugLength=1
        
        self.larvaePositions={} #a dictionary that names the larvae so that each can be accessed separately: {key, value} = {index: [node, position, angle, condition, movement]}
        #node is used to acces delete bug or change its model, and condition refers to either "larvae" or "bug", used to check if the size
        #still needs to be increased, and movement refers to either "move" or "not moved" so that each bug moves towards the worm only once

        self.applePositions={} #a disctionary that names the apples so that each can be accesed: {key, value}= {key:value}={index:[node, coordinate]}

        self.startg = 0 #is set to 1 once the game has started
        self.changing_camera_pos=False #This tells weather or not to put the camera in a new position
        #If the camera has been changed manually, this becomes false, to stop the camera from automatically being

        # Initialize the ShowBase class from which we inherit, which will
        # create a window and set up everything we need for rendering into it.
        ShowBase.__init__(self)

        self.postTitle()
    
        self.postInstructions()

        self.keyCommands()

        # Disable the mouse to discard movement of camera with mouse
        self.disableMouse()

        # Set the Camera position and where it looks- this places the camera to the right and slighlty above the scene of apples and
        #the position of where the worm starts at (ie (0,0,0)) and it roughly looks at that position
        camera.setPosHpr(5, -5, 1, 35, -10, 0)

        #to smoothen the camera view we find the average coordinates of the last ten camera positions
        self.cameraposList=[] #this is the list of camera positions which is added to each iteration.

        self.lighting()

        # Global Time
        self.dt = globalClock.getDt()

        # Initialize score of player
        self.score = 0
        # an array to store the (x,y,z) coordinates of the centre of the circles that make up the worm
        self.centre_circle = []

        # an array to store the (x,y,z) coordinates of the points on the edge of the circle. Each item is a list of length 3 
        #representing a on the circle. 10 points make up one circle, so every ten points is a new circle
        self.coord_circle = []

        # value of worm's steering wheel
        self.steer_alpha = 0.0
        self.steer_beta=0
        #value that controlls speed of worm
        self.worm_speed=2

        # Radius of the worm
        self.wormRadius = 0.02 #this is reduced at the head and tail, but for the body it should be the same.

        self.wormMaterial()

        self.c = 0
        self.vn = "wormShape" + str(self.c)

        def ModelSet(name="set1"):
            return NodePath(name)

        self.set1 = ModelSet("set1")
        self.set1.reparentTo(self.render)
        
        self.loadGround()

        self.loadSky()
 
        self.spawnRadius=5 #the spawn radius in units of the worm tells us how spread out the positions of the bugs and apples are allowed to be

        self.load_Initial_Apples()

        
        for i in range (0,5): #loads an initial five larvae
            self.loadLarvae(i)

        # create empty worm object
        self.nWorm = None

    #a function that returns the normal of a point on the circle with coordinates self.coord_circle[A]
    def Normal(self, A):
        #To work out the normal at each point find the vector from the centre of the circle to the point- 
        # this is along the radius so is perpendicular to the surface. Then divide by length to get the normal vector
        ANormal=(-self.centre_circle[A]+self.coord_circle[A])
        return Point3(tuple(ANormal/linalg.norm(ANormal)))
    
    #adds the coordinates of a triangle to the geom whose coordinates are the i, j and kth positions in the array self.coord_circle
    def set_of_triangles(self, i, j, k, vertex):

        #adds the positions of the points on the triangle
        vertex.addData3(Point3(tuple(self.coord_circle[i])))
        vertex.addData3(Point3(tuple(self.coord_circle[j])))
        vertex.addData3(Point3(tuple(self.coord_circle[k])))
    
    #adds the normal vecotrs of the points on a triangle to the geom whose coordinates are the i, j and kth positions in the array self.coord_circle
    def normals_to_set(self,i, j, k, normal):
        #adds normal vectors to vdata
        normal.addData3(self.Normal(i))
        normal.addData3(self.Normal(j))
        normal.addData3(self.Normal(k))

    def color_of_set(self, i, j, k, color, colour):
        #Adds colour to a set of vectors that form a triangle. 
        #colour must be a list of length 4 representing the RGBA values
        color.addData4(colour[0], colour[1], colour[2], colour[3])
        color.addData4(colour[0], colour[1], colour[2], colour[3])
        color.addData4(colour[0], colour[1], colour[2], colour[3])

    def create_wormShape(self):
        # set the format for the shape
        # v3n3c4 asks for 3 vectors and 3 normals and 4 colours
        _format = GeomVertexFormat.get_v3n3c4() #specifies the number of arrays needed for each vertex- in our cse three vertices, three normals and three colour arrays.
        vdata = GeomVertexData(self.vn, _format, Geom.UHStatic) #defines the numeric vertex data with the format _format and the name self.vn
        #Geom.UHStatic simply tells panda we will not be changing the vertices

        texcoord = GeomVertexWriter(vdata, 'texcoord') #we need to use GeomVertexWriters to fill in the data, one for each column
        vertex = GeomVertexWriter(vdata, "vertex") #then by later changing these veriables, they will be stored in the variable vdata
        normal = GeomVertexWriter(vdata, "normal")
        color = GeomVertexWriter(vdata, 'color')

        triangle = GeomTriangles(Geom.UHStatic) #creates a GeomPrimitive object, which in this case needs three vertices to be added

        k0 = 0 #these are the vertices that will be added. The actual positions of these veertices depends on the values of
        k2 = 2 #k0, k1 and k2 in the associates GeomVertexData which is the variable called vdata
        k1 = 1

        # now add the points to the vertices
        num_worm_rings=929 #this is the number of points used to created the connected triangle mesh of the worms body
        
        for i in range(num_worm_rings):
            try:
                # First set of triangles (tip points towards the tail)

                #adds the positions of the points on the triangle
                self.set_of_triangles( i, i+10, i+1, vertex)

                #adds normal vectors to vdata
                self.normals_to_set( i, i+10, i+1, normal)

                #To create a stripe, we want every tenth triangle to be coloured differently. To make sure that this tenth triangle is on
                #the top of the worm at the beggining, the index must have remainder 5 when divided by 10 (remainder 0 is the first 
                # index of the every circle, and this occurs on the underside of the worm at its starting position)
                if i%10==5:
                    #an RGBA value of (0.5, 0.25, 0.25. 0 )creates a dark brown- to make more visible, use a bright red: (1, 0, 0, 0)
                    self.color_of_set(i, i+10, i+1, color,(0.5,0.25, 0.25,0))
                else:
                    #An RGBA value of (0.25,0.25,0.25,0) creates a dark grey colour- this is the main colour of the worm
                    self.color_of_set(i, i+10, i+1, color,(0.25,0.25,0.25,0))

                triangle.addVertices(k0, k1, k2) #This adds ONE triangle
            except IndexError:
                continue

            k0 = k0 + 3 #because we have just used the previouse 3 vectors, so we now need to use the consecutive 3 vectors
            k1 = k1 + 3
            k2 = k2 + 3
            try:
                #Second set of triangles facing the opposite way (tip points towards head)

                #adds the positions of the points on the triangle
                self.set_of_triangles( i+1, i+10, i+11, vertex)

                #adds normal vectors to vdata
                self.normals_to_set( i+1, i+10, i+11, normal)

                if i%10==5:
                    self.color_of_set(i+1, i+10, i+11, color,(0.5,0.25,0.25,0))
                else:
                    self.color_of_set(i+1, i+10, i+11, color,(0.25,0.25,0.25,0))

                triangle.addVertices(k0, k1, k2)
            except IndexError:
                continue

            k0 = k0 + 3
            k1 = k1 + 3
            k2 = k2 + 3

        # Create the shape
        wormShape = Geom(vdata) 
        wormShape.addPrimitive(triangle)

        return wormShape

    #A method that creates the x,y and z coordinates of the points on the circle of the worm
    def x_y_z(self, circleCount, centreOfCircle, vector1, vector2, theta, Radius):
        coordinates=[]
        for i in range (0,3): #becuase there are three components, (x,y,z)
            coordinates.append(centreOfCircle[circleCount][i] + (vector1[circleCount][i] * cos(theta) + vector2[circleCount][i] * sin(theta)) * Radius)

        return coordinates #returns a list representing [x,y,z] the coordinates of a point on the the ende of the circle with coorinates centreOfCircle[CircleCount]
        #and e1=vecotor 1 and e2=vector 2 and with an angle theta separating them

    def radius(self, circleCount): #calculates the radius of the circle that is at index circleCount along the centre line of the worm
        #the head of the worm is at index 30, the tail is at index 0
        if circleCount > self.start_of_head: #if the circle count is inside the head, we want to reduce the radius evenly
            if circleCount > self.end_of_head: #this is the curved bit at the very end of the worm so the head is curved not pointy
                interval=self.number_of_circles-self.start_of_head #used to calulate by how much the radius should reduce, since 
                #we want the radius to reduce evenly until it reaches zero. 
                radiusReduction=0.5*self.wormRadius/(interval)#0.5*self.wormRadius is the radius at the 'end' of the head, ie the end of the slowly sloping section before the very sloped section
                Radius=(0.5*self.wormRadius)-(radiusReduction*(circleCount-self.end_of_head)) #we need to interval-(self.number_of_circles-circleCount)
                #becasue the head is at the end of the worm with the highest index count
            else: #THis is the slowly sloping section of the head
                interval=self.number_of_circles-self.end_of_head
                radiusReduction=0.5*self.wormRadius/(interval)
                Radius=self.wormRadius-(radiusReduction*(circleCount-self.start_of_head))
        elif circleCount< self.start_of_tail: #the tail should be pointy, so the radius reduces uniformly
            interval=self.start_of_tail
            radiusReduction=self.wormRadius/(interval)
            Radius=self.wormRadius-(radiusReduction*(interval-circleCount))
        else:
            Radius=self.wormRadius
        return Radius

    def newCircle(self, circleCount, centreOfCircle, vector1, vector2): #appends the lists of points to include all the 10 points on one circle
        # number of points for a circle
        numPointsOnCircle = 10
        
        appendcentre = self.centre_circle.append #this is used to work out the normal vectors
        appendcoord = self.coord_circle.append #appends the coordinates of each point that will make up a triangle to the list self.coord_circle
        
        Radius=self.radius(circleCount)

        for i in range(numPointsOnCircle): 
            # angle between two points
            theta = 2 * pi * i / numPointsOnCircle
            
            # the coordinates for each point are added to the array
            appendcoord(self.x_y_z(circleCount, centreOfCircle, vector1, vector2, theta, Radius)) 
            
            #The coordinates for the centre of the circle are added to the array self.centre_circle (used to work out normal vectors)
            appendcentre(centreOfCircle[circleCount])
    
    def updateCoordinates(self, alpha_offset, beta_offset, speed):
        alpha_pref = Expression("v", t=self.nWorm.t, v=alpha_offset, degree=1)
        gamma_pref = Expression("0.0", degree=0)
        beta_pref = Expression("A*sin(2.0*pi*(x[0]-f*t))+v",f=speed, A=5, t=self.nWorm.t, v=beta_offset, degree=1)
        # f controlls the speed of the movement, A controlls the amplitude of the oscillation
        controls = Controls(
            alpha=v2f(alpha_pref, fs=self.nWorm.V),
            beta=v2f(beta_pref, fs=self.nWorm.V),
            gamma=v2f(gamma_pref, fs=self.nWorm.Q),)

        # Update worm with new control to return new values
        self.nWorm.update_solution(controls)
    
    def bugCollision(self, centreOfCircle, circleCount):
        #checks if the point on the worm is touching any of the bugs using the function above, is_object_touching_bug
        for index, bug in self.larvaePositions.items():
            if bug[3]=="bug" and self.is_object_touching_bug(bug[1], centreOfCircle[circleCount], bug[2]*(pi/180), self.bugWidth, self.bugLength, self.bugHeight) == True:
                
                #The condition for checking if the point x is inside the cuboid. We only want to check if it is a bug and not if it is a larvae
                print("you have touched the bug")
                print("Game over")
                print("Your final score is:", self.score)
                sys.exit() #when a bug is touched, the score is shown and then the game exits immedeatly
            elif ((bug[3]=="bug") and 
            (self.is_object_touching_bug(bug[1], centreOfCircle[circleCount], bug[2]*(pi/180), self.bugWidth*5, self.bugLength*5, self.bugHeight*5) == True)
            and (bug[4] == "not moved")): #THis last condition is necesarry so that the bug only moves towards the worm once
                    vector=centreOfCircle[circleCount]-bug[1] #the vector that goes from the bug to the worm
                    finalPosition=bug[1]+0.25*vector #bug will move quarter of the way towards the worm
                    bug[0].setPos(Point3(tuple(finalPosition))) #Sets the position of the bug to the new position, which is closer towards the worm
                    self.larvaePositions[index]=[bug[0], finalPosition, bug[2], "bug", "moved"] #updates the dictionary so it contains the new coordinate

    def removeApple(self, index):
        #removes apple in question
        nodeToBeRemoved=self.applePositions[index][0]  #self.applePositions is a dictionary where {key:value}={i,[node, coordinate]}, so for the key i,
        #the value is a list who's first index is the node and whos second index is the coordinate
        nodeToBeRemoved.removeNode() #removes node

    def appleCollision(self, centreOfCircle, circleCount, applePositionsCopy):
        
        for index, apple in applePositionsCopy.items(): #splits up each key:value pair in the dictionary applePositionsCopy, so we can acess each indeividually. 
            #apple in applePositionsCopy is a list whos first item is the node of that apple and whose second is the coordinates for that apple  
            
            if linalg.norm(centreOfCircle[circleCount]-list(apple[1]))<0.3: #checks the coordinates of that apple against the centre of the circle of the worm
                #so baisically checks if they are too close

                self.removeApple(index)
            
                self.score+=1
                self.scorePost.destroy()
                self.postScore()
                self.spawnRadius+=1 #increases the spawn radius so worm has to travel further and less crowded
                
                self.loadLarvae(len(self.larvaePositions))

                self.worm_speed+=0.5

                self.addApple(index) #adds a apple and appends it to the dictionary with key index
                
                for index, bug in self.larvaePositions.items(): #So that if the worm comes close to the same bug, it will be able to move again
                    bug[4]="not moved"
                
                return True #if a apple has been hit, method exits immedeatly and the variable hit is set to true
        return False #If that circle is not  within collision of a apple or bug, hit is set to false and the next circle on the snake is checked


    #A method that checks if the worm has collided with a bug or a apple and responds accordingly
    def checkForCollision(self, centreOfCircle,vertex1, vertex2 ):

        applePositionsCopy=self.applePositions.copy() #by using the copied version to check for apple positions, we avoid aliasing
            #by making a copy before we enter the for loop, it avoids checking the newly created apples which are added to self.applePositions within the for loop

        for circleCount in range(0,len(centreOfCircle)):
            #finds the new coordinates of the points on the end of the circle which has index circle
            self.newCircle(circleCount, centreOfCircle, vertex1, vertex2)

            #checks if worm has hit a bug, and system exits if this is true. Avoids unnecessary checks by putting it first
            self.bugCollision(centreOfCircle, circleCount)

            #checks if worm has hit a apple. Hit is set as the return value. If hit is True, loop exits and no more points are checked
            hit=self.appleCollision(centreOfCircle, circleCount, applePositionsCopy)
            if hit==True: #This makes it exit the entire function.
                return hit           
        return hit
            

    def updateWorm(self, alpha_offset= 0.0, beta_offset = 0, speed=2):

        # clear the arrays for the points of the new worm
        self.coord_circle.clear()
        self.centre_circle.clear()

        self.updateCoordinates(alpha_offset,beta_offset, speed)

        centreOfCircle = []
        vertex1 = []
        vertex2 = []
        
        # Add the outputs of the update function to the arrays
        centreOfCircle = self.nWorm.get_x().T
        vertex1 = self.nWorm.get_e1().T
        vertex2 = self.nWorm.get_e2().T

        # set the camera to look at the maximum x, y z position- the camera actuallly looks at an average of 10 camera positions

        self.camerapos=(amax(centreOfCircle[:,0]), amax(centreOfCircle[:,1]), amax(centreOfCircle[:,2]) )
        

        #checks if the worm has collided with a bug or an apple and responds accordingly
        self.checkForCollision( centreOfCircle,vertex1, vertex2)

        gnode2 = GeomNode(self.vn)
        # add the geom shape to the node and at the node to the main renderer
        gnode2.addGeom(self.create_wormShape())
        self.wNode = self.render.attachNewNode(gnode2)

        camera.lookAt(self.wNode)
        self.wNode.setMaterial(self.myMaterial)

    # Records the state of the arrow keys
    def setKey(self, key, value):
        self.keyMap[key] = value

    def move(self, task):

        # Get the time that elapsed since last frame.  We multiply this with
        # the desired speed in order to find out with which distance to move
        # in order to achieve that desired speed.
        self.dt = globalClock.getDt()
        dt = globalClock.getDt()
        

        if self.keyMap["start_game"] and self.startg == 0: #only responds to the h key if the game has not yet started
            # call the worm class from simulator file
            self.postScore()

            self.nWorm = Worm(self.number_of_circles, self.dt)
            self.nWorm.initialise()
            self.updateWorm(0)
            self.wNode.removeNode()
            self.removeInstructions()           
            self.startg = 1 #game has started 

        if self.startg == 1:
            #we only want the programme to respond to key commands if the game has started

            # turn camera left
            if self.keyMap["cam_left"]:
                self.changing_camera_pos=True
                self.camera.setX(self.camera, -10 * dt)

            # turn camera right
            if self.keyMap["cam_right"]:
                self.changing_camera_pos=True
                self.camera.setX(self.camera, +10 * dt)
            
            # Zoom in
            if self.keyMap["zoom_in"]:
                self.changing_camera_pos=True
                self.camera.setY(self.camera, +10 * dt)

            # Zoom out
            if self.keyMap["zoom_out"]:
                self.changing_camera_pos=True
                self.camera.setY(self.camera, -10 * dt)
            
            # Move camera up
            if self.keyMap["look_up"]:
                self.changing_camera_pos=True
                if self.camera.getZ() < 5:
                    self.camera.setZ(self.camera, +10 * dt)

            # Move camera down
            if self.keyMap["look_down"]:
                self.changing_camera_pos=True
                if self.camera.getZ() > 0.5:
                    self.camera.setZ(self.camera, -10 * dt)
            
            # worm turns right
            if self.keyMap["mright"]:

                self.c = self.c + 1
                self.vn = "wormShape" + str(self.c)
                self.steer_beta=2
                    

            # worm turns left
            if self.keyMap["mleft"]:

                self.c = self.c + 1
                self.vn = "wormShape" + str(self.c)
                self.steer_beta=-2

            #sets steering to zero when no keys are being pressed so that the worm does not travel in circles
            if not self.keyMap["mright"] and not self.keyMap["mleft"]:
                self.steer_beta=0
            
            #worm moves up
            if self.keyMap["mup"]:

                self.c = self.c + 1
                self.vn = "wormShape" + str(self.c)
                # increase right steering until maximum value
                if self.steer_alpha > -2:
                    self.steer_alpha -= 1
        
            # worm moves down
            if self.keyMap["mdown"]:

                self.c = self.c + 1
                self.vn = "wormShape" + str(self.c)
                # increase left steering until maximum value
                if self.steer_alpha < 2:
                    self.steer_alpha += 1
                    
            #sets steering to zero when no keys are being pressed so that the worm does not travel in circles
            if not self.keyMap["mup"] and not self.keyMap["mdown"]:
                self.steer_alpha=0

        if self.nWorm is not None:

            #increases the size of the larvae until they are big enough to turn into bugs, then replaces them with bugs
            for key, bug in self.larvaePositions.items(): 
                if bug[3] == "larvae": #We only want to increase the size of the model if it is still a larvae (the bugs dont grow)
                    if bug[0].getScale()[0]<0.2: 
                        #bug is a list of length two, with the first item being the node that gives all the info about the bug
                        scale=bug[0].getScale()[0]+0.002
                        bug[0].setScale(scale,scale,scale) 
                    else: #If the larvae has reached a certain size, we want it to change into a bug
                        self.loadBug(bug[0], key, bug[1], bug[2])
            
            # remove previous worm
            self.wNode.removeNode()

            self.updateWorm(self.steer_alpha, self.steer_beta , self.worm_speed)
                    
            self.cameraposList.append(self.camerapos) #adds each (x,y,z) coordinate to the list of camera positions
            x=average(array(self.cameraposList)[-10:,0]) #finds the average of the last 10 x coordinates
            y=average(array(self.cameraposList)[-10:,1]) #finds the average of the last 10 y coordinates
            z=average(array(self.cameraposList)[-10:,2]) #finds the average of the last 10 z coordinates
            #camera.lookAt is necessary otherwise the camera looks at (0,0,0) all the time
            camera.lookAt(x,y,z) #to smoothen the camera view we look at the average of the last ten coordinates- reduces the effects of small movements
            #setting the camera (x,y,z) positions is necesarry to make the camera follow the worm, otherwise the worm gets very far in the distance
            
            if self.changing_camera_pos == False: #only changes camera position if the camera has NOT been changed manually
                self.camera.setX(x+4)
                self.camera.setY(y-3)
                self.camera.setZ(z+1) #If I want the camera to stay at the same height (might make it easier for orientation purposes)
            #then self.camera.setZ(3)

        return task.cont


demo = WormDemo()
demo.run()