from simpy import *

theta = symbols('theta')

def func(t):
    return cos(t)

print(diff(func(theta), theta))