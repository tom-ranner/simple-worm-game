[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

# Installation instructions:

1. Create a conda environment with [Conda](https://conda.io/docs/) using the environment file provided:

```bash
$ conda env create -f environment.yml

```

2. At the command prompt type `conda activate simple-worm-game`.
3. Then type `python wormPanda.py` to run the game

# Game Instructions:

1. Press H for the game to start
2. A,W,D,X,S,Z manipulate the camera
3. J,L turn the worm left and right
The instructions above are also printed at the top left of the game interface

# Game Objective:

Collect the suns to gather points and avoid the moons or else you will lose the game. Careful! The more suns you get the more moons spawn.

# Licence

This project is licenced under the GNU General Public Licence v3.0 - see the [LICENCE](LICENSE) file for details.

    Copyright (C) 2020-2021 Stefanos Panagiotou
    Copyright (C) 2021      Thomas Ranner
    Copyright (C) 2021      Esther Delap

